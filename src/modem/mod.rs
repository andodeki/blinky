//! Responses for General Commands
use atat::heapless::String;
use atat::atat_derive::AtatUrc;

use atat::atat_derive::{AtatCmd, AtatResp};

#[derive(Clone, AtatResp)]
pub struct NoResponse;

#[derive(Clone, AtatCmd)]
#[at_cmd("", NoResponse, timeout_ms = 1000)]
pub struct AT;

#[derive(Clone, AtatUrc)]
pub enum Urc {
    #[at_urc("+UMWI")]
    MessageWaitingIndication(MessageWaitingIndication),
}


#[derive(Clone, AtatResp)]
pub struct MessageWaitingIndication;




/// 4.1 Manufacturer identification
/// Text string identifying the manufacturer.
#[derive(Clone, Debug, AtatResp)]
pub struct ManufacturerId {
    pub id: String<64>,
}

/// Model identification
/// Text string identifying the manufacturer.
#[derive(Clone, Debug, AtatResp)]
pub struct ModelId {
    pub id: String<64>,
}

/// Software version identification
/// Read a text string that identifies the software version of the module.
#[derive(Clone, Debug, AtatResp)]
pub struct SoftwareVersion {
    pub id: String<64>,
}

/// 7.11 Wi-Fi Access point station list +UWAPSTALIST
#[derive(Clone, Debug, AtatResp)]
pub struct WifiMac {
    pub mac_addr: atat::heapless_bytes::Bytes<12>,
}

#[derive(Clone, Debug, AtatResp)]
pub struct ATCOMMAND {
    pub id: String<64>,
}

// use atat::atat_derive::AtatCmd;
// use responses::*;

/// 4.1 Manufacturer identification +CGMI
///
/// Text string identifying the manufacturer.
#[derive(Clone, AtatCmd)]
#[at_cmd("+CGMI", ManufacturerId)]
pub struct GetManufacturerId;

/// Model identification +CGMM
///
/// Read a text string that identifies the device model.
#[derive(Clone, AtatCmd)]
#[at_cmd("+CGMM", ModelId)]
pub struct GetModelId;

/// Software version identification +CGMR
///
/// Read a text string that identifies the software version of the module
#[derive(Clone, AtatCmd)]
#[at_cmd("+CGMR", SoftwareVersion)]
pub struct GetSoftwareVersion;

/// 7.12 Wi-Fi MAC address +UWAPMACADDR
///
/// Lists the currently used MAC address.
#[derive(Clone, AtatCmd)]
#[at_cmd("+UWAPMACADDR", WifiMac)]
pub struct GetWifiMac;


/// 7.12 check if device ok AT
///
/// Lists the currently used MAC address.
#[derive(Clone, AtatCmd)]
#[at_cmd("", ATCOMMAND)]
pub struct GetAT;