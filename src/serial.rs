use embassy_time::{Timer, Duration};
use hal::{peripherals::UART1,  gpio::GpioPin};
use hal::{
    uart::{
        config::{Config, DataBits, Parity, StopBits},
        TxRxPins,
    },
    Uart, clock::Clocks
};
use embassy_sync::{blocking_mutex::raw::NoopRawMutex, signal::Signal};
use esp_hal_common::uart::config::AtCmdConfig;
use esp_hal_common::uart::{ UartRx, UartTx};
use esp_backtrace as _;
use static_cell::make_static;

#[allow(dead_code)]
const READ_BUF_SIZE: usize = 64;

#[allow(dead_code)]
const AT_CMD: u8 = 0x04;

pub fn uart_serial_setup(gpio1: GpioPin<hal::gpio::Unknown, 15>, gpio3: GpioPin<hal::gpio::Unknown, 14>, uart0_per: UART1, clocks: &Clocks<'static>) -> (
    UartTx<'static, UART1>, 
    UartRx<'static, UART1>, 
    &'static Signal<NoopRawMutex, usize>,
){
    let config = Config {
        baudrate: 115200,
        data_bits: DataBits::DataBits8,
        parity: Parity::ParityNone,
        stop_bits: StopBits::STOP1,
    };

    let pins = TxRxPins::new_tx_rx(
        gpio1.into_push_pull_output(),
        gpio3.into_floating_input(),
    );

    let mut serial1 = Uart::new_with_config(uart0_per, config, Some(pins), &clocks);
    serial1
        .set_rx_fifo_full_threshold(READ_BUF_SIZE as u16)
        .unwrap();
    let (tx15, rx14) = serial1.split();

    let signal = &*make_static!(Signal::new());

    (tx15, rx14, signal)
    
}

#[embassy_executor::task]
pub async fn at_writer(mut tx: UartTx<'static, UART1>, signal: &'static Signal<NoopRawMutex, usize>) {
    use core::fmt::Write;
    // embedded_io_async::Write::write(
    // // embedded_io_async::Write::write_all(
    //     &mut tx,
    //     b"AT+CGMI",
    //     // b"AT with EOT (CTRL-D).\r\n",
    // )
    // .await
    // .unwrap();

    embedded_io_async::Write::write(&mut tx, b"AT+CGMI").await.unwrap();
    // embedded_io_async::Write::write_all(cmd).await.map_err(|_| Error::Write)?;

    // for _ in 0..3 {
    //     match embedded_io_async::Write::write_all(&mut tx, b"AT+CGMI").await {
    //         Ok(_r) => {},//esp_println::println!("SIZE RESPONSE: {:?}", r),
    //         // Err(atat::Error::Timeout) => {},
    //         Err(_) => log::error!("Write Error"),
    //     };
    //     // embedded_io_async::Write::flush(&mut tx).await.unwrap();
    // }


    embedded_io_async::Write::flush(&mut tx).await.unwrap();
    // loop {
    //     let bytes_read = signal.wait().await;
    //     signal.reset();
    //     write!(&mut tx, "\r\n-- received {} bytes --\r\n", bytes_read).unwrap();
    //     embedded_io_async::Write::flush(&mut tx).await.unwrap();
    // }
}
#[embassy_executor::task]
pub async fn at_reader(mut rx: UartRx<'static, UART1>, signal: &'static Signal<NoopRawMutex, usize>) {
    const MAX_BUFFER_SIZE: usize = 100 * READ_BUF_SIZE + 16;

    let mut rbuf: [u8; MAX_BUFFER_SIZE] = [0u8; MAX_BUFFER_SIZE];
    let mut offset = 0;
    loop {
        let r = embedded_io_async::Read::read(&mut rx, &mut rbuf[offset..]).await;
        match r {
            Ok(len) => {
                offset += len;
                // if &rbuf[..offset] == b"OK\r\n" {
                //     esp_println::println!("Received OK response");
                // } else if &rbuf[..offset] == b"+CME ERROR: 58\r\n" {
                //     esp_println::println!("Received Error response: 58");
                // }

                // +CME ERROR: 58
                let data_str = core::str::from_utf8(&rbuf[..offset]).unwrap_or("Invalid UTF-8");
                // log::info!("Read: {}, data: {}", len, data_str);
                if data_str.contains("OK") {
                    esp_println::println!("Received OK response");
                } else if data_str.contains("+CME ERROR: 58") {
                    esp_println::println!("Received Error response: 58");
                } else {
                    // Timer::after(Duration::from_millis(3000)).await;
                    esp_println::println!("Read: {}, data: {}\n", len, data_str);
                }
                // esp_println::println!("Read: {}, data: {}\n", len, data_str);
                // esp_println::println!("Read: {len}, data: {:?}", &rbuf[..offset]);
                offset = 0;
                signal.signal(len);
            }
            Err(e) => esp_println::println!("RX Error: {:?}", e),
        }
    }
}